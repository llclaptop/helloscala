object PrintEvenNumber {
  def main(args: Array[String]) {
    val evenNumList = (0 to 100).filter(_ % 2 == 0)
    evenNumList.foreach(arg => print(arg+" "))
    println()
  }
}
